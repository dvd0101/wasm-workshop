# Introduzione a WebAssembly

## Cos'è WebAssembly

La definizione ufficiale (che trovate su
[https://webassembly.org/](https://webassembly.org/)) è

> WebAssembly (abbreviated Wasm) is a binary instruction format for a
> stack-based virtual machine. Wasm is designed as a portable target for
> compilation of high-level languages like C/C++/Rust, enabling deployment on
> the web for client and server applications.

Da questa descrizione deduciamo che con il termine WASM si può indicare:

- un formato di file binario
- il bytecode per una cpu virtuale (stack-based)
- l'output di un compilatore

La versione 1.0 di questa tecnologia è stata pubblicata, da un Working Group
del W3C, nel Febbraio 2018, ed è implementata dai quattro browser princiali; in
altre parole, ad oggi, è utilizzabile (senza utilizzare polyfill) dal
[76.85%](https://caniuse.com/#search=wasm) degli utenti mondiali.

Un altro modo per definire WebAssembly potrebbe essere: "lo stack tecnologico
che permette di eseguire codice binario precompilato all'interno di una
macchina virtuale JS".

È comprensibile se le parole "codice binario" e "web" (ok questa non c'era)
insieme nella stessa frase evocano gli spettri di un passato mai troppo remoto
(Flash, Acrobat Reader, Applet Java).

Ma WASM è figlio del nuovo web delle single page application, quando è chiaro
ormai da anni che un browser è uno dei componenti software più critici per
quanto riguarda la sicurezza, essendo esposto alla più grande superficie
d'attacco mai incontrata.

Le specifiche WebAssembly sono state pensate per impedire, **by design**, tutti
i veicoli di attacco più comuni, e per impedire che codice malevolo abbia
accesso ai dati esterni alla sandbox in cui viene eseguito.

## A cosa serve

Perché utilizzare WebAssembly? Perché aggiungere un nuovo elemento alla già
infinita toolchain richiesta per lo sviluppo di una applicazione web moderna?

Si utilizza WASM per due motivi:

  1. performance
  2. riutilizzo del codice

### Performance

Il bytecode WASM è eseguito all'interno della virtual machine JS, ma non è né
interpretato né converito in javascript. Viene invece compilato, strumentato e
ottimizzato per la vera cpu sottostante; ed il codice risultante può essere
esguito direttamente senza il controllo dell'interprete.

Le prestazioni variano ovviamente da caso d'uso a caso d'uso, più il compito è
intensivo per cpu e memoria più il guadagno è notevole; nei casi ottimi si può
avere un tempo di esecuzione di poco superiore a quello nativo (<2x).

### Riutilizzo del codice

Ad oggi esistono quattro linguaggi main stream che possono essere compilati per
WASM: C, C++, Rust e Go (dalla versione 1.11, ancora experimental).

Questo significa che qualunque libreria, algoritmo o pezzo di codice scritto in
uno di questi linguaggi diventa potenzialmente riutilizzabile da un
applicazione web.

Quanto sia facile riutilizzare il codice esistente varia, anche stavolta, da
caso a caso; ma se il codice da ricompilare non fa assunzioni particolari di
basso livello (come ad esempio un emulatore) la probabilità che il codice
compili così com'è è quasi del 100%.

Per semplificare [il
porting](http://kripken.github.io/emscripten-site/docs/porting/index.html) di
codice esistente, il compilatore C/C++ Emscripten reimplementa alcune funzioni
della libc (filesystem, pthread) e alcune librerie famose (SDL, OpenGL)
utilizzando le API messe a disposizione dal browser.


### A cosa non serve

WebAssembly ad oggi non è un modo per scrivere un'intera applicazione web. Il
suo scopo non è sostituire React, Angular o manipolare il DOM.

Può ovviamente interagire con il codice di una pagina web, chiamare funzioni JS
ed essere chiamato da esse, ma la sua raison d'être non è scrivere interfacce
utente.

O almeno non interfacce utente "canoniche"; un approccio interessante, sebbene
il risultato sia una UI un po' "aliena", è quello intrapreso dalle Qt.

Qt sono un toolkit grafico per creare applicativi desktop e dal 2018 possono
essere compilate in WASM; qui c'è un programma [di
esempio](http://example.qt.io/qt-webassembly/SensorTagDemo/SensorTagDemo.html);
l'interfaccia è ovviamente atipica, ma è lo stesso identico codice che può
essere compilato per Linux, OS/X, Windows e ora WASM.

## Come funziona

Per poter eseguire del codice WebAssembly l'ambiente di esecuzione (browser o
Node)
[deve](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WebAssembly):

  1. Caricare il file con l'output del compilatore in un ArrayBuffer
  2. Compilare l'ArrayBuffer in un'istanza di `WebAssembly.Module`
  3. Istanziare il `WebAssembly.Module` per ottenere un `WebAssembly.Instance`

L'ArrayBuffer con il bytecode può essere recuperato in qualsiasi modo, anche
con una semplice `fecth`.

Anche compilare il programma è un operazione semplice; la funzione da
utilizzare `WebAssembly.compile` richiede un unico argomento, l'ArrayBuffer con
il bytecode, e restituisce una `Promise` che verrà risolta con il modulo
compilato.

La compilazione del bytecode si occupa di:

  - validare il programma WASM
  - generare codice macchina ottimizzato per la cpu corrente

La sicurezza e le performance di WebAssembly sono ottenute da questi due
passaggi.
Il primo esegue una verifica formale assicurando che il codice sia
staticamente corretto (che le funzioni siano chiamate con il giusto numero di
argomenti, che lo stack non sia corrompibile, etc).
Il secondo passaggio genera codice ottimizzato e aggiunge i controlli di
sicurezza che possono essere fatti solo a runtime.

Per instanziare (fase di link) un modulo si utilizza la funzione
`WebAssembly.Instance` che ha bisogno di due argomenti: un `WebAssembly.Module`
e un "_import object_", ed è qui che le cose diventano un po' complicate.

Senza entrare troppo nel [dettaglio](https://webassembly.org/docs/modules/)
possiamo semplificare dicendo che un modulo WebAssembly non contiene la
definizione di tutto ciò di cui ha bisogno; ad esempio il classico "hello
world" in C:

    #include <stdio.h>

    void f() {
      puts("Hello World\n");
    }

È compilato nel seguente WAT (la rappresentazione testuale di WASM):

    (module
     (type $FUNCSIG$ii (func (param i32) (result i32)))
     (import "env" "puts" (func $puts (param i32) (result i32)))
     (table 0 anyfunc)
     (memory $0 1)
     (data (i32.const 16) "Hello World\n\00")
     (export "memory" (memory $0))
     (export "f" (func $f))
     (func $f (; 1 ;)
      (drop
       (call $puts
        (i32.const 16)
       )
      )
     )
    )

Il formato sexprs di WAT è sufficientemente leggibile da farci capire che la
funzione `f` chiama una funzione `$puts` di cui non è presente la definizione;
è invece presente una dichiarazione preceduta dalla parola chiave "import".

Per creare un'istanza di questo modulo e poter chiamare `f` dobbiamo fornire a
`WebAssembly.Instance` un oggetto con tutti gli import richiesti, nel caso
specifico potremmo definire una funzione `puts` che scrive nella console del
browser.

I simboli importabili (ed esportabili) da un modulo non sono solo funzioni, ma
possono essere aree di memoria o tabelle (utilizzate per implementare il
caricamento dinamico).

Data la difficoltà di fornire un import object corretto i vari compilatori
generano anche il codice js necessario a compilare ed instanziare il modulo
WASM, nella grande maggioranza dei casi non c'è bisogno di altro.

### Linear memory

Ogni modulo WASM ha accesso a una o più zone di memoria (che come abbiamo visto
possono essere definite internamente o fornite dalla Virtual Machine JS);
queste zone di memoria possono crescere al bisogno, però è necessario che siano
continue.

La versione 1.0 di WASM non permette di avere una zona di memoria "segmentata";
questo semplifica l'implementazione del compilatore (i controlli a runtime sono
semplici) e massimizza le performance in quanto la memoria assegnata può essere
ottenuta direttamente dal sistema operativo con una semplice malloc.

Questa proprietà della memoria ha anche un altro vantaggio; può essere esposta
alla VM JS come un semplice `Int8Array`.

### Moduli e Worker

Come abbiamo visto un `WebAssembly.Module` è il risultato della compilazione
del bytecode WASM, oltre ad essere instanziabile e permettere al codice JS di
chiamarne le funzioni esportate ha un'altra caratteristica; è completamente
stateless e supporta lo [structural
cloning](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Structured_clone_algorithm).

Questo significa che può essere condiviso, a differenza delle sue istanze, tra
[Web Worker](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API).

### Perché va veloce

Le prestazioni ottenibili da WebAssembly hanno origine da due fattori.

Il primo è che l'assembler prodotto dai compilatori WASM (che poi i browser
ricompilano in codice nativo) è stato studiato per essere semplice (ad esempio
prevede solo [quattro tipi di
dato](https://webassembly.org/docs/semantics/#Types)) e per avere una semantica
comune a tutte le cpu moderne (o almeno a quelle più diffuse).

Il secondo punto è particolarmente importante per garantire sia una
compilazione che un esecuzione efficiente.

L'altro fattore che contribuisce in maniera fondamentale è il tipo di linguaggi
che utilizziamo per generare il nostro WebAssembly.

Javascript è un linguaggio semplice ma ha una semantica molto ricca; prendiamo
come esempio:

    function sum(a, b) {
      return a + b;
    }

Secondo
[specifiche](https://www.ecma-international.org/ecma-262/5.1/#sec-11.6.1)
sommare due entità in JS significa eseguire il seguente algoritmo (a runtime):

1. Let lref be the result of evaluating AdditiveExpression.
2. Let lval be GetValue(lref).
3. Let rref be the result of evaluating MultiplicativeExpression.
4. Let rval be GetValue(rref).
5. Let lprim be ToPrimitive(lval).
6. Let rprim be ToPrimitive(rval).
7. If Type(lprim) is String or Type(rprim) is String, then
     Return the String that is the result of concatenating ToString(lprim) followed by ToString(rprim)
8. Return the result of applying the addition operation to ToNumber(lprim) and ToNumber(rprim).

Un altro esempio interessante sono le chiamate a metodi di isntanze, che
richiedono una serie di lookup in hash table solo per decidere quale funzione
deve essere chiamata.

C, C++, Rust e Go sono linguaggi più complessi ma che hanno una semantica, a
runtime, molto più semplice; una volta che il compilatore ha fatto il suo
lavoro una somma si traduce in un singolo opcode mentre una chiamata a funzione
(quando non viene eliminata dagli ottimizzatori) risulta in una manciata di
istruzioni.

Se, per esempio, potessimo compilare Python (un linguaggio con una complessità
semantica simile a quella di Javascript) in WASM il risultato sarebbe
interessante, ma sicuramente non per le prestazioni.

Per inciso esistono ricompilazioni di CPython in WASM, ma ovviamente il codice
python eseguito in questo modo risulta più lento che se eseguito nativamente.

### Le parti difficili...

Quando si pianifica di utilizzare WASM per permettere ad un applicazione JS di
utilizzare funzioni scritte in un altro linguaggio bisogna ricordarsi che
stiamo per affrontare due tipi di problemi non banali:

  1. cross-compiling
  2. generazione di binding cross-language

Dobbiamo cross-compilare codice probabilmente testato su una sola architettura
(al 99% x86) per una cpu virtuale con tutti i problemi che questo comporta:

  - tooling diverso o insufficiente
  - assunzioni (arbitrarie) che vengono meno
  - cicli di test che si allungano

Dobbiamo generare i binding cross-linguaggio per permettere al JS di chiamare
le nostre funzioni; come detto più sopra WASM conosce solo 4 tipi (due interi e
due in virgola mobile) tutto il resto deve essere costruito su questi.

Ad esempio, per esporre questo codice C++ a JS:

    class T {
      public:
        T(int);

        int get() const;

      private:
        int value;
    };

    int sum(T, T);

Dobbiamo scrivere questo _glue-code_:

    // bindings.cpp
    #include <emscripten/bind.h>
  
    using namespace emscripten;
  
    EMSCRIPTEN_BINDINGS(library) {
        class_<T>("T")
          .constructor<int>()
          .function("get", &T::get);
  
        function("sum", &sum);
    }

Fortunatamente siamo nel 2018 e gli strumenti a disposizione sono molto buoni.

Sia il compilatore C/C++ che quello Rust si basano su LLVM un progetto che non
ha certo bisogno di presentazioni; i problemi che si incontrano durante la
cross-compilazione  sono da imputare ai limiti della piattaforma piuttosto che
a bug del compilatore.

La scrittura dei binding è un operazione che deve essere fatta a mano, non è
semplicissima né esente da errori, ma i tool a disposizione aiutano molto e
risultano banali per i casi semplici.

Per C++ esistono
[embind](http://kripken.github.io/emscripten-site/docs/porting/connecting_cpp_and_javascript/embind.html)
e
[WebIDL](http://kripken.github.io/emscripten-site/docs/porting/connecting_cpp_and_javascript/WebIDL-Binder.html),
per Rust c'è
[wasm-bindgen](https://rustwasm.github.io/wasm-bindgen/introduction.html)
mentre per Go si può usare syscall/js.

### ...e quelle brutte

Mentre la VM Javascript ha accesso sia alla propria memoria che a quella
utilizzata da un modulo WASM, il codice eseguito da quest'ultimo, per motivi
sia di sicurezza che di prestazioni, può accedere solo alla propria.

Immaginiamo di aver il seguente codice compilato come un modulo WASM:

    class Image;
  
    // the returned image is a blend of the two arguments
    Image blend(Image const& i1, Image const& i2);


Una volta caricato, compilato e istanziato il modulo come possiamo chiamare la
funzione blend da JS?

Se proviamo nel modo in cui siamo abituati:

    // mod is the WASM module instance
    console.log(mod.blend(new Image(/*...*/), new Image(/*...*/)));

Ecco che abbiamo ottenuto qualcosa di nuovo, un **memory-leak**!

Si perché, purtroppo, interfacciarsi con codice WASM significa, per adesso,
dover gestire manualmente l'allocazione e deallocazione della memoria; niente
GC né distruttori che possano aiutarci.

Se la funzione `blend` deve poter accedere ai dati delle due immagini, queste
devono essere allocate nella memoria assegnata al modulo WASM; questa
allocazione viene fatta dalla new (il browser sa dove quel particolare tipo di
oggetto deve essere allocato).

Ma una volta che non servono più dobbiamo informare manualmente il runtime WASM
che quegli oggetti non sono più necessari in modo che possa riutilizzare la
memoria assegnata, e dobbiamo farlo prima che il garbage collector distrugga i
riferimenti a quelle istanze mantenuti nel runtime JS.

Nella chiamata a `mod.blend` ci sono tre memory leak, il modo corretto di
scrivere quel codice è:

    // allocate
    const img1 = new Image(/*...*/);
    const img2 = new Image(/*...*/);

    // use
    const blended = mod.blend(img1, img2);
    console.log(blended);

    // deallocate
    img1.free();
    img2.free();
    blended.free();

Ugh gestione della memoria C-style in un'applicazione web del 2018!

La situazione ad oggi è questa, ed è un compromesso tecnico imposto dal fatto
che in JS [non è possibile
sapere](https://github.com/WebAssembly/design/issues/238) quando un oggetto
viene reclamato dal garbage collector.

## Che cosa sta per arrivare

La prima versione di WebAssembly è stata disegnata con l'obbiettivo di essere
un Minimum Viable Product; doveva fornire il set minimo di strumenti necessari
a provarne il valore.

Adesso l'evoluzione procede con lo sviluppo delle feature che non sono
rientrate, per un motivo o per l'altro, nel primo rilascio.

[L'elenco delle feature attualmente in
sviluppo](https://webassembly.org/docs/future-features/) contiene molte cose
interessanti, ma tra queste ne elenco due:

1. [Threads](https://github.com/WebAssembly/design/issues/1073)
2. [Garbage Collection](https://github.com/WebAssembly/design/issues/1079)

La prima si occupa di aggiungere le primitive base per poter avere codice
multi-thread all'interno di un singolo modulo e di poter sfruttare (e farci
male con) la programmazione concorrente a memoria condivisa.

È un'aggiunta molto richiesta, sopratutto per poter sfruttare codice esistente
multi-thread. Al momento l'unico modo per utilizzare più processori è
istanziare lo stesso modulo su più `WebWorker`, ma il modello di concorrenza
implementato da questi è quello di task a memoria non condivisa, e il codice
deve sincronizzarsi scambiandosi messaggi.

La seconda feature è incentrata sul fornire gli strumenti di basso livello per
poter far ispezionare il codice WASM al garbage collector della VM host.

L'obbiettivo finale è quello di rendere la compilazione per WebAssembly di
linguaggi garbage collected estremamente efficiente, ed evitare che
quest'ultimi debbano includere una copia del proprio run-time (come fa Go 1.11)
in ogni modulo WASM.  Se funzionerà, in futuro si spera che molti più linguaggi
offrano come opzione la possibilità di compilare per WebAssembly.
