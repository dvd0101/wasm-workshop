#include <cstdint>
#include <numeric>
#include <emscripten/emscripten.h>

extern "C" {
	void EMSCRIPTEN_KEEPALIVE grayscale(uintptr_t ptr, size_t length) {
		auto bytes = reinterpret_cast<uint8_t*>(ptr);
		for (size_t ix = 0; ix < length; ix += 4) {
			const int avg = (bytes[ix] + bytes[ix + 1] + bytes[ix + 2]) / 3.0;
			bytes[ix + 0] = avg;
			bytes[ix + 1] = avg;
			bytes[ix + 2] = avg;
		}
	}
}
