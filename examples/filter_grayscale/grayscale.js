function grayscale(imgData) {
  const bytes = imgData.data;
  for (let ix = 0; ix < bytes.length; ix += 4) {
    const avg = (bytes[ix] + bytes[ix + 1] + bytes[ix + 2]) / 3;
    bytes[ix] = avg;
    bytes[ix + 1] = avg;
    bytes[ix + 2] = avg;
  }
}

