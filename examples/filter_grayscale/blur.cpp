namespace {
      const int kernel_size = 7;
      const std::array<float, kernel_size * kernel_size> kernel = {
        0.00000067, 0.00002292, 0.00019117, 0.00038771, 0.00019117, 0.00002292, 0.00000067,
        0.00002292, 0.00078633, 0.00655965, 0.01330373, 0.00655965, 0.00078633, 0.00002292,
        0.00019117, 0.00655965, 0.05472157, 0.11098164, 0.05472157, 0.00655965, 0.00019117,
        0.00038771, 0.01330373, 0.11098164, 0.22508352, 0.11098164, 0.01330373, 0.00038771,
        0.00019117, 0.00655965, 0.05472157, 0.11098164, 0.05472157, 0.00655965, 0.00019117,
        0.00002292, 0.00078633, 0.00655965, 0.01330373, 0.00655965, 0.00078633, 0.00002292,
        0.00000067, 0.00002292, 0.00019117, 0.00038771, 0.00019117, 0.00002292, 0.00000067,
	  };
}

void blur(uintptr_t ptr, int width, int height) {
	auto bytes = reinterpret_cast<uint8_t*>(ptr);

	std::array<float, kernel.size()> pixels;
	const int kHalf = kernel_size / 2;
	float kSum = std::accumulate(kernel.begin(), kernel.end(), 0);
	kSum = 0;
	for (int ix = 0; ix < kernel.size(); ix++)
		kSum += kernel[ix];

	auto fill_kernel_pixels = [&](int row, int col) {
		for (int pr = -kHalf; pr <= kHalf; pr++) {
		for (int pc = -kHalf; pc <= kHalf; pc++) {
			const int pxCoord = (pr + kHalf) * 3 * kernel_size + (pc + kHalf) * 3;

			const int imRow = row + pr;
			const int imCol = col + pc;
			if (imRow < 0 || imCol < 0 || imRow >= height || imCol >= width) {
			pixels[pxCoord + 0] = 0;
			pixels[pxCoord + 1] = 0;
			pixels[pxCoord + 2] = 0;
			} else {
			const int imCoord = imRow * 4 * width + imCol * 4;
			pixels[pxCoord + 0] = bytes[imCoord + 0];
			pixels[pxCoord + 1] = bytes[imCoord + 1];
			pixels[pxCoord + 2] = bytes[imCoord + 2];
			}
		}
		}
	};

	auto calculate = [&]() {
	  std::array<float, 3> sum = {0, 0, 0};
		for (size_t ix = 0; ix < kernel.size(); ix++) {
		sum[0] += pixels[ix * 3 + 0] * kernel[ix];
		sum[1] += pixels[ix * 3 + 1] * kernel[ix];
		sum[2] += pixels[ix * 3 + 2] * kernel[ix];
		}
		return sum;
	};

	for (int row = 0; row < height; row++) {
		for (int col = 0; col < width; col++) {
		fill_kernel_pixels(row, col);
		const int imCoord = row * 4 * width + col * 4;
		const auto value = calculate();
		bytes[imCoord + 0] = value[0] / kSum;
		bytes[imCoord + 1] = value[1] / kSum;
		bytes[imCoord + 2] = value[2] / kSum;
		}
	}

}
