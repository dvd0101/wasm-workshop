class Grayscale {
  process(r, g, b) {
    const avg = (r + b + g) / 3;
    return [avg, avg, avg];
  }
}

function process(image, filters) {
  const bytes = image.data;
  for (let ix = 0; ix < bytes.length; ix += 4) {
    let pixel = [bytes[ix], bytes[ix+1], bytes[ix+2]];
    for (const filter of filters) {
      pixel = filter.process(...pixel);
    }
    bytes[ix + 0] = pixel[0];
    bytes[ix + 1] = pixel[1];
    bytes[ix + 2] = pixel[2];
  }
}
