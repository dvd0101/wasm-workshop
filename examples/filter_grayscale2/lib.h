#include <cstdint>
#include <vector>

namespace lib {
	struct color {
		uint8_t r;
		uint8_t g;
		uint8_t b;
	};

	class filter {
		public:
			virtual ~filter() = default;
			virtual color process(color) const = 0;
	};

	class grayscale : public filter {
		public:
			color process(color) const override;
	};

	void process(uint8_t* pixels, int size, std::vector<filter*> const&);
}
