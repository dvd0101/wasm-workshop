#include "lib.h"
#include <emscripten/bind.h>

using namespace emscripten;

void process(uintptr_t ptr, int size) {
	auto pixels = reinterpret_cast<uint8_t*>(ptr);

	lib::grayscale f_gray;
	lib::process(pixels, size, {&f_gray});
}

EMSCRIPTEN_BINDINGS(lib) {
	function("process", &process);
}
