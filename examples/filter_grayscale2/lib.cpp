#include "lib.h"
#include <cstdint>

namespace lib {
	color grayscale::process(color c) const {
		uint8_t avg = (c.r + c.b + c.g) / 3.0;
		return {avg, avg, avg};
	}

	void process(uint8_t* pixels, int size, std::vector<filter*> const& filters) {
		for (int ix = 0; ix < size; ix += 4) {
			color pixel{pixels[ix], pixels[ix + 1], pixels[ix + 2]};
			for (auto& filter : filters) {
				pixel = filter->process(pixel);
			}
			pixels[ix + 0] = pixel.r;
			pixels[ix + 1] = pixel.g;
			pixels[ix + 2] = pixel.b;
		}
	}
}
