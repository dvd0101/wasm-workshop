#include "lib.h"
#include <emscripten/bind.h>

using namespace emscripten;

EMSCRIPTEN_BINDINGS(lib) {
	function("draw", &draw);
}
