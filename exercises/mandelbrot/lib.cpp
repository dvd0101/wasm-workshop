#include "lib.h"

#include <algorithm>
#include <complex>
#include <stdexcept>

namespace {
	int const max_iters = 1000;

	int escape_iters(std::complex<float> const& c) {
		std::complex<float> z(0, 0);
		int iters = 0;
		while (iters < max_iters && std::norm(z) < 4) {
			z = z * z + c;
			iters++;
		}
		return iters;
	}

	void draw(uint8_t* pixels, int width, int height, std::complex<float> center, float radius) {
		auto const scale = std::min(2 * radius / width, 2 * radius / height);

		auto const top_left = center + std::complex<float>(
			(-width / 2) * scale,
			(height / 2) * scale
		);

		auto point = top_left;
		auto const y_offset = std::complex<float>(0, scale);

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int const iters = escape_iters(point);

				int const i = y * width * 4 + x * 4;
				if (iters == 1000) {
					pixels[i + 0] = 0;
					pixels[i + 1] = 0;
					pixels[i + 2] = 0;
				} else {
					pixels[i + 0] = static_cast<uint8_t>(iters);
					pixels[i + 1] = static_cast<uint8_t>(iters * 0.6);
					pixels[i + 2] = static_cast<uint8_t>(iters * 0.2);
				}
				pixels[i + 3] = 255;
				point += scale;
			}
			point -= y_offset;
			point.real(top_left.real());
		}
	}
}

void draw(uintptr_t ptr, int size, float center_x, float center_y, float radius, int width, int height) {
	if (size != width * height * 4) {
		throw std::runtime_error("the buffer does not match the image size");
	}
	auto pixels = reinterpret_cast<uint8_t*>(ptr);
	draw(pixels, width, height, std::complex<float>(center_x, center_y), radius);
}
