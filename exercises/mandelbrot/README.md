# Esercizio #2

La libreria **lib.cpp** contienere una reimplemenrtazione del frattale di
Mandelbrot scritto in JS nel file **mandelbrot.js**.

La libreria è completa e per compilarla si usa **make**.

Lo scopo dell'esercizio è imparare a condividere un buffer di memoria allocato
dal JS; per farlo si deve reimplementare le funzioni `allocate` e `release`.

Vi è un ulteriore quesito, opzionale, che consiste nell'ottimizzare la
costruzione di un oggetto in modo da eliminare una copia inutile; cercate il
commento "TODO - Improvement".

Per testare il codice possiamo usare **emrun**:

    $ emrun index.html

Risorse:

- [Rappresentazione della memoria](https://kripken.github.io/emscripten-site/docs/porting/emscripten-runtime-environment.html#emscripten-memory-representation) di emscripten
- [Reference TypedArray](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypedArray);

Consigli:
- Utilizzate la console del browser ed "esplorate" con l'istanza `Module`.
- Non è necessario implementare tutti i *TODO* nel codice per cominciare a vedere qualcosa.
