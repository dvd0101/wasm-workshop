em++ -s WASM=1 -std=c++14 -O3 -Wall -Wextra -ffast-math -c lib.cpp -o lib.o
em++ -s WASM=1 --bind -std=c++14 -O3 -Wall -Wextra -ffast-math -c bind.cpp -o bind.o
em++ -s WASM=1 --bind -std=c++14 -O3 -Wall -Wextra -ffast-math lib.o bind.o -o lib.js
