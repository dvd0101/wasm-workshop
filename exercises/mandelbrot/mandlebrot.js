const maxIters = 1000;
function _escapeIters(px, py) {
  let zx = 0;
  let zy = 0;
  let iters = 0;
  while (iters < maxIters && (zx * zx + zy * zy) < 4) {
    const temp = zx * zx - zy * zy + px;
    zy = 2 * zx * zy + py;
    zx = temp;
    iters++;
  }
  return iters;
}

function drawMandlebrot(center, radius, width, height) {
  const img = new ImageData(width, height);

  const eps = Math.min(2 * radius / width, 2 * radius / height);
  const topLeft = [
    center[0] - width / 2 * eps,
    center[1] + height / 2 * eps,
  ];

  for (let y = 0; y < height; y++) {
    const rowValue = [ topLeft[0], topLeft[1] - y * eps ];
    for (let x = 0; x < width; x++) {
      rowValue[0] += eps;
      const iters = _escapeIters(...rowValue);

      const i = y * width * 4 + x * 4;
      if (iters === 1000) {
        img.data[i + 0] = 0;
        img.data[i + 1] = 0;
        img.data[i + 2] = 0;
        img.data[i + 3] = 255;
      } else {
        img.data[i + 0] = iters * 1;
        img.data[i + 1] = iters * 0.6;
        img.data[i + 2] = iters * 0.2;
        img.data[i + 3] = 255;
      }
    }
  }
  return img;
}
