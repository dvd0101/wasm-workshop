# Esercizio #1

La libreria **lib.cpp** contienere una reimplemenrtazione del parser e
dell'indice implementati in **index_js.js**.

Lo scopo dell'esercizio è scrivere i binding in modo che la pagina di test
**index.html** esegua entrambi gli algoritmi (quello scritto in JS e quello in
C++).

Per scrivere i binding utilizziamo [embind](https://kripken.github.io/emscripten-site/docs/porting/connecting_cpp_and_javascript/embind.html).

Per testare il codice possiamo usare **emrun** un'utility di emscripten:

    $ emrun index.html


Il progetto è già configurato con un Makefile; i binding vanno scritti nel file
**bind.cpp**.

Alcune risorse utili:

- Documentazione di [embind](https://kripken.github.io/emscripten-site/docs/porting/connecting_cpp_and_javascript/embind.html)
- Conversioni [builtin](https://kripken.github.io/emscripten-site/docs/porting/connecting_cpp_and_javascript/embind.html#built-in-type-conversions) tra JS e C++ (prestare attenzione alla factory _register_vector_)
