em++ -s WASM=1 -s ASSERTIONS=2 --bind -std=c++14 -Wall -Wextra -O3 -c lib.cpp -o lib.o && ^
em++ -s WASM=1 -s ASSERTIONS=2 -std=c++14 -Wall -Wextra -O3 -c bind.cpp -o bind.o && ^
em++ -s WASM=1 -s ASSERTIONS=2 --bind -Wall -Wextra -O3 lib.o bind.o -o lib.js
