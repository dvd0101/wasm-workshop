#include "lib.h"
#include <algorithm>

namespace {
	constexpr char const delimiters[] = " .,;\r+*[]_\"'<>";

	bool is_delimiter(char c) {
		using namespace std;
		return find(begin(delimiters), end(delimiters), c) != end(delimiters);
	}
}

namespace lib {

	void index::add(std::string const& word, position pos) {
		auto it = _index.find(word);
		if (it == _index.end()) {
			_index.insert({word, std::vector<position>{pos}});
		} else {
			it->second.push_back(pos);
		}
	}

	std::vector<std::string> index::freqs() const {
		std::vector<std::string> words;
		for (auto const& it : _index) {
			words.push_back(it.first);
		}

		using namespace std;
		sort(
			begin(words),
			end(words),
			[this](auto& a, auto& b) {
				const int a_count = _index.at(a).size();
				const int b_count = _index.at(b).size();
				return b_count < a_count;
			});
		return words;
	}

	std::vector<position> index::positions(std::string const& word) const {
		auto it = _index.find(word);
		if (it == _index.end()) {
			return {};
		}
		std::vector<position> pos = it->second;
		using namespace std;
		sort(
			begin(pos),
			end(pos),
			[](auto& a, auto& b) {
				return a.pos < b.pos;
			});
		return pos;
	}

	index parse(std::string const& text) {
		index result;

		std::string word = "";
		size_t pos = 0;
		int row = 0;
		int col = 0;

		auto add_word = [&]() {
			if (word.size() > 0) {
				result.add(word, position{static_cast<int>(pos), row, col});
			}
		};

		for (; pos < text.size(); pos++) {
			auto const ch = text[pos];
			if (ch == '\n') {
				add_word();
				row++;
				col = 0;
				continue;
			}

			if (is_delimiter(ch)) {
				add_word();
				word = "";
			} else {
				word += ch;
			}
			col++;
		}

		return result;
	}

}
