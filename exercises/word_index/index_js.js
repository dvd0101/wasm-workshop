class Position {
  constructor(p, r, c) {
    this.pos = p || 0;
    this.row = r || 0;
    this.col = c || 0;
  }
}

class Index {
  constructor() {
    this.words = new Map();
  }

  add(word, pos) {
    const positions = this.words.get(word);
    if (positions === undefined) {
      this.words.set(word, [ pos ]);
    } else {
      positions.push(pos);
    }
  }

  freqs() {
    const words = [...this.words.keys()];
    words.sort((a, b) => this.words.get(b).length - this.words.get(a).length);
    return words;
  }

  positions(word) {
    const pos = [...this.words.get(word)];
    return pos.sort((a, b) => a.index - b.index);
  }
}

function isDelim(c) {
  return c === " " || c === "." || c === "," || c === ";" || c === "\r" ||
         c === "+" || c === "*" || c === "[" || c === "]" || c === "_" ||
         c === "\"" || c === "'" || c === "<" || c === ">";
}

function jsParse(text) {
  const index = new Index();

  let word = "";
  let pos = 0;
  let row = 0;
  let col = 0;

  const addWord = () => {
    if (word) {
      index.add(word, new Position(pos, row, col));
    }
  };

  for (; pos < text.length; pos++) {
    const ch = text[pos];
    if (ch === "\n") {
      addWord();
      row++;
      col = 0;
      continue;
    }

    if (isDelim(ch)) {
      addWord();
      word = "";
    } else {
      word += ch;
    }
    col++;
  }

  return index;
}
