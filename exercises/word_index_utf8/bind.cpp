#include "lib.h"
#include <emscripten/bind.h>

using namespace emscripten;

auto parseAsciiString(uintptr_t ptr, unsigned int length) {
	auto str = reinterpret_cast<const char*>(ptr);
	return lib::parse({str, length});
}

EMSCRIPTEN_BINDINGS(lib) {
	register_vector<std::string>("VectorString");
	register_vector<lib::position>("VectorPosition");

	class_<lib::index>("Index")
		.constructor<>()
		.function("freqs", &lib::index::freqs)
		.function("positions", &lib::index::positions);


	function("parse", &parseAsciiString);
}
