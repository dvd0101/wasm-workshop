#pragma once
#include <string>
#include <unordered_map>
#include <vector>

namespace lib {
	struct position {
		int pos;
		int row;
		int col;
	};

	class index {
		public:
			void add(std::string const&, position);

			std::vector<std::string> freqs() const;
			std::vector<position> positions(std::string const&) const;

		private:
			std::unordered_map<std::string, std::vector<position>> _index;
	};

	index parse(std::string const&);
}
