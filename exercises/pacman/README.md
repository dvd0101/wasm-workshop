# Esercizio - Pacman

In questa directory c'è un emulatore del gioco originale pacman (l'emulatore è
stato copiato da [qui](https://github.com/rasky/pacman/tree/step13)].

Lo scopo dell'esercizio è modificare l'emulatore perché compili con emscripten
e possa essere usato in un browser.

Il progetto utilizza CMake.

Alcuni indizi:

- Emscripen fornisce la definizione della toolchain per CMake nel file **Emscripten.cmake** da usare insieme alla variabile **-DCMAKE_TOOLCHAIN_FILE**
- La [documentazione](https://kripken.github.io/emscripten-site/docs/porting/emscripten-runtime-environment.html) su come fare il porting di applicazioni, in particolare le sezioni **File System** e **Browser main loop**
- Per fare generare un file **.html** a CMake utilizzate il comando **set(CMAKE_EXECUTABLE_SUFFIX ".html")**
