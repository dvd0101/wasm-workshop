#!/usr/bin/python3
import re
import sys


class _Slide:
    def __init__(self, options=None):
        self.children = []
        self.text = ""
        self.slide_options = options or ""

    def append(self, slide):
        self.children.append(slide)
        return slide

    def add_text(self, t):
        self.text += t


class SlideHtml(_Slide):
    def __repr__(self):
        return f"<SlideHtml: ({len(self.children)})>"

    def render(self, indent=""):
        if self.children:
            text = f"<section>\n{self.text}\n</section>\n"
            text += "\n".join(s.render(indent=indent + "  ") for s in self.children)
        else:
            text = self.text
        return f"<section {self.slide_options}>\n{text}\n</section>\n"


class SlideMarkdown(_Slide):
    def __repr__(self):
        return f"<SlideMarkdown: ({len(self.children)})>"

    def render(self, indent=""):
        text = f"""
        <section data-markdown>
            <textarea data-template {self.slide_options}>
                {self.text}
            </textarea>
        </section>
        """
        if len(self.children):
            children = "\n".join(s.render() for s in self.children)
            return f"<section {self.slide_options}>{text}{children}</section>"
        else:
            return text


def parse(fobj):
    """
    Parse the file object and returns an AST
    """
    slide_header = re.compile(r"(>{1,2})\s*(html)?(.*)")
    slides = []
    root = None
    current = None
    for line in fobj:
        m = slide_header.match(line)
        if m:
            slide_type = m.group(1)
            slide_class = SlideHtml if m.group(2) == "html" else SlideMarkdown
            slide_options = m.group(3)
            # if current Is None a child slide is promoted to a root.
            if slide_type == ">>" and root is not None:
                current = root.append(slide_class(options=slide_options))
            else:
                root = current = slide_class(options=slide_options)
                slides.append(current)
        elif current is not None:
            current.add_text(line)
    return slides


def render(slides, template, out):
    """
    Renders the slides using the template file object.
    Output is written to the out file object.
    """
    fragment = "\n".join([s.render() for s in slides])
    boilerplate = template.read()
    html = re.sub(r"<div class=\"slides\">.*</div>",
                  f"<div class=\"slides\">{fragment}</div>",
                  boilerplate,
                  flags=re.DOTALL)
    out.write(html)


slides = parse(open(sys.argv[1]))
render(slides, open(sys.argv[2]), sys.stdout)
